# Projet_devOps_Signorino_Flament

Projet évalué : Architecture DevOps
--------------------------------------------------------------------------------

                               Informations

Port de connexion Apache : 8080
/!\ Modifiable avec la variable d'environment du .env : PORTAPACHE

Port de connexion BDD : 8081
/!\ Modifiable avec la variable d'environment du .env : PORTBDD

--------------------------------------------------------------------------------

                                Installation

Etape 1 : <br/>
- git clone https://gitlab.com/ludovic.flament38projet_devops_signorino_flament.git <br/><br/>

Etape 2 : <br/>
- cd www/ <br/>
- git clone https://github.com/digininja/DVWA.git <br/>
- cd ../ <br/>

Etape 3 :
- sudo docker build -t dvwa . <br/>
/!\ Pensez à bien indiquer le "." à la fin de la ligne <br/>

<br/>
Etape 3 :
- sudo docker-compose up -d <br/>

Etape 4 :
- Lancer un navigateur et charger http://localhost:port/DVWA/setup.php
/!\ Port étant par défaut à 8080 <br/>
