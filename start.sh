#!/bin/bash

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
service mysql start

cp /var/www/html/php.ini /etc/php/7.3/apache2/php.ini
cp /var/www/html/config.inc.php /var/www/html/DVWA/config

service apache2 restart

while true
do
    tail -f /var/log/apache2/*.log
    exit 0
done
