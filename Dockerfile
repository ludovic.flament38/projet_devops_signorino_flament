FROM debian:latest

RUN apt update && apt upgrade -y && \
    apt install -y \
    mariadb-server \
    php-mysql \
    apache2 \
    php \
    php-mysqli \
    php-gd \
    libapache2-mod-php \
    git

RUN chown www-data:www-data -R /var/www/html/

COPY start.sh /
ENTRYPOINT ["/start.sh"]
